import pandas as pd
import numpy as np
import math
import random

p_plus = 0.5149384885764499
p_minus = 0.48506151142355014

class Tree(object):
    def __init__(self,column_no, children, children_nodes, is_leaf, decision):
        self.column_no = column_no
        self.children = children
        self.children_nodes = children_nodes
        self.is_leaf = is_leaf
        self.decision = decision

def equal(a,b):
    if ( a == b ):
        return True
    else:
        return False

def is_in_class( class_data , real_data ):
    if real_data <= class_data[1] and real_data >= class_data[0]:
        return True
    else:
        return False

no_of_bins = 40
def get_class_comparision_fun(column):
    if column in [0,3,4,5,6,8,9,11,12]:
        return equal
    else:
        return equal

def classes_in_column(column):
    if column == 0:
        return ['b','a']
    elif column == 3:
        return ['u', 'y', 'l', 't']
    elif column == 4:
        return ['g', 'p', 'gg']
    elif column == 5:
        return ['c', 'd', 'cc', 'i', 'j', 'k', 'm', 'r', 'q', 'w', 'x', 'e', 'aa', 'ff']
    elif column == 6:
        return ['v', 'h', 'bb', 'j', 'n', 'z', 'dd', 'ff', 'o']
    elif column == 12:
        return ['g', 'p', 's']
    elif column in [8,9,11]:
        return ['t','f']
    else:
        return [i for i in range(1,no_of_bins+1)]

def get_probabilities_for_classes_in_column(column, data_frame):
    classes = classes_in_column(column)
    classes_prob = [ len(data_frame[data_frame[column] == classes[i]])/float(len(data_frame)) \
                    for i in range(len(classes))]
    return classes_prob

def get_distribution_for_column(column, data_frame):
    comparision_function = get_class_comparision_fun(column)
    #Different Classes in The Column
    classes = classes_in_column(column)
    #Positive Examples in The Class
    class_pos = [ 0 for i in range(len(classes))]
    #Negative Examples in The Class
    class_neg = [ 0 for i in range(len(classes))]

    for row in data_frame.index:
        for j in range(len(classes)):
            if comparision_function(classes[j], data_frame[column][row]):
                if ( data_frame[15][row] == '+' ):
                    class_pos[j] += 1
                else:
                    class_neg[j] += 1
    k = 1
    for j in range(len(classes)):
        if class_neg[j] + class_pos[j] == 0:
            class_pos[j] = 0.5
        else:
            class_pos[j] = (class_pos[j]+k)/float(class_neg[j]+class_pos[j]+2*k)
    return class_pos

def calculate_entropy(X):
    entropy=0
    for p in X:
        if p != 0:
            entropy += -p * math.log(p,2)
    return entropy

def calculate_entropy_for_column(column,data_frame):
    P = get_distribution_for_column(column , data_frame)
    entropy = 0
    classes_prob = get_probabilities_for_classes_in_column(column, data_frame)

    for i in range(len(P)):
        entropy += classes_prob[i]*calculate_entropy([P[i], 1-P[i]])

    return entropy

def assign_interval ( column_min , column_max , no_of_intervals, data):
    interval_size = (column_max - column_min)/no_of_intervals
    for interval in range(1,no_of_intervals+1):
        if data >= column_min + (interval-1)*interval_size and data <= column_min + interval*interval_size:
            return interval

def assign_intervals_to_column(data_frame,column,no_of_intervals):
    column_min = data_frame[column].min()
    column_max = data_frame[column].max()

    for row in data_frame.index:
        #Assign a number to data_frame[column][row]
        interval = assign_interval( column_min , column_max , no_of_intervals, data_frame[column][row])
        data_frame[column][row] = interval
    return data_frame

def load_data_frame(data_path):
    columns = [i for i in range(16)]
    training_data = pd.read_csv(data_path, names=columns)
    rows_to_be_deleted  = []
    for column in training_data:
        for i in range(len(training_data)):
            if training_data[column][i] == '?':
                rows_to_be_deleted.append(i)
                continue
    #The Rows with Incomplete Data are removed
    training_data = training_data.drop(training_data.index[rows_to_be_deleted])

    #Fixing Some Continuous Values which are taken as input as string
    training_data[[1]] = training_data[[1]].astype(np.float64)
    training_data[[13]] = training_data[[13]].astype(np.float64)
    columns_with_continuous_values = [1,2,7,13,14]
    for column in columns_with_continuous_values:
        training_data = assign_intervals_to_column (training_data, column, no_of_bins)
    #print training_data.head(3)
    return training_data

def choose_highest_IG_column(data_frame):
    #Don't Know how to to deal with 0 Entries Data

    p = len(data_frame[data_frame[15] == '+'])/float(len(data_frame))
    #cur_entropy = calculate_entropy([p, 1-p])
    entropy = [-1 for i in range(16)]

    columns_done = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14]
    for column in data_frame:
        if column in columns_done:
            entropy[column] = calculate_entropy_for_column(column,data_frame)
    #print entropy
    #print entropy.index(min(entropy))
    min_value =  min([n for n in entropy if n >= 0])
    return entropy.index(min_value)

#x = x.drop(0, 1)
def build_decision_tree(data_frame,depth):
    no_of_pos = len(data_frame[data_frame[15] == '+'])
    no_of_neg = len(data_frame[data_frame[15] == '-'])
    if len(data_frame.columns) == 1 or no_of_pos == 0 or no_of_neg == 0 or depth > 8:
        decision = '+'
        no_of_pos = no_of_pos + 1
        no_of_neg = no_of_neg + 1

        if ( no_of_pos*p_plus < no_of_neg*p_minus ):
            decision = '-'
        leaf_node = Tree(15, [], [], True, decision)
        return leaf_node
    #No Training Samples in This direction
    if ( len(data_frame) == 0 ):
        return None
    column = choose_highest_IG_column(data_frame)
    children = classes_in_column(column)
    children_nodes_data_frame = [ data_frame[data_frame[column] == child] for child in children]
    children_nodes_data_frame = [ child_data_frame.drop(column,1) for child_data_frame in children_nodes_data_frame]
    children_nodes = [build_decision_tree(child_node_data_frame,depth+1) for child_node_data_frame in children_nodes_data_frame]
    cur_node = Tree(column, children, children_nodes, False, 'x')
    return cur_node

def test_sample_on_decision_tree(decision_tree,testing_sample):
    if decision_tree.is_leaf == True:
        return decision_tree.decision
    column = decision_tree.column_no
    #Value of that column and use that child_node
    for i in range(len(decision_tree.children)):
        if testing_sample[column] == decision_tree.children[i]:
            if decision_tree.children_nodes[i] != None:
                return test_sample_on_decision_tree(decision_tree.children_nodes[i],testing_sample)
            else:
                return '+'

def test_with_decision_tree(decision_tree, testing_data):
    correctly_classified = 0
    for row in testing_data.index:
        testing_sample = testing_data.ix[row]
        true_label = testing_sample[15]
        classified_label = test_sample_on_decision_tree(decision_tree,testing_sample)
        if true_label == classified_label:
            correctly_classified += 1
    print correctly_classified/float(len(data_frame))

data_frame = load_data_frame('crx.data.txt')
trained_decision_tree = build_decision_tree(data_frame[0:500],1)
test_with_decision_tree(trained_decision_tree, data_frame[501:])

def depth_of_tree(node):
    if node == None:
        return 0
    depth = 0
    for child in node.children_nodes:
        depth = max(1+depth_of_tree(child),depth)
    return depth

depth_of_tree(trained_decision_tree)
